/*jslint browser: true*/
/*global $*/

$('input#userName').on('focusin', function () {
	$('#nameWarning').slideDown(500);
});

$('input#userName').on('focusout', function () {
	$('#nameWarning').hide();
});

$('input').attr('autocomplete','off');

$('button#next').click(function () {
	var userName = $('#userName').val();
	var userBirth = $('#userBirth').val();
	var userType = $('input[name=userType]:checked').val();	
	
	function checkAge () {
		var inputDate = new Date($('#userBirth').val());

			age = Math.abs( // это небольшое решение с подсчетом возраста я взяла из сети
				 new Date(
					   Date.now() - inputDate
				  ).getUTCFullYear() - 1970
			 );

		return(age);
	}
	
	if (!userName || !userBirth) {
		$("#errorMessage1").html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Не все данные указаны.');
		$("#errorWarning").slideDown();
	} else if (!userType) {
		$("#errorMessage1").html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Не выбран статус.');
		$("#errorWarning").slideDown();
	} else {
		var age;
		checkAge();
		if (age <= 10) {
			$("#errorMessage1").html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Извините, сервис для вас ещё не доступен. (Ограничение возраста)');
			$("#errorWarning").slideDown();
		} else {
			$("#errorWarning").slideUp();
			displayFormBlock (userType);
		}		
	}	
});

function displayFormBlock (userType) {
	if (userType == 'currentUser') {
		$("#typeOfUserWrapper").slideUp(500);
		$('#currentUserWrapper').slideDown(500);
	} else if (userType == 'newUser') {
		$("#typeOfUserWrapper").slideUp(500);
		$('#newUserWrapper').slideDown(500);
	}
}


// для существующего пользователя:

$('button.back').click(function () {
	$('#currentUserWrapper').slideUp(500);
	$('#newUserWrapper').slideUp(500);
	$('#typeOfUserWrapper').slideDown(500);
});

$('#logIn').click(function () {
	var userEmail = $('#userEmail').val();
	var userPass = $('#userPass').val();
	
	function isValidEmailAddress(userEmail) {
    var emailPattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return emailPattern.test(userEmail);
    }	
	
	if (!userEmail || !userPass) {
		$('#errorMessage2').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Не все данные указаны.');
		$('#errorWarningCurrentUser').show();
	} else if (userEmail) {
		if (!isValidEmailAddress(userEmail)) {
			$('#errorMessage2').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Email введен некорректно. Используйте формат example@domain.smth');
			$('#errorWarningCurrentUser').show();
		} else {
			$('#errorWarningCurrentUser').hide();
			$('#successWarningCurrentUser').show();
			setTimeout(function() {
				$('#successWarningCurrentUser').fadeOut('fast')
			}, 2000);
			// далее проверка на наличие записи в БД и тд
		}
	}
});


// для нового пользователя:

$('#street').on('keyup', function () {
	var search = $('#street').val();
	var searchPattern = new RegExp(search, 'i');
	$.getJSON('data/street-list.json', function (data) {
		var output = '<ul>';
		$(data.adresses).each(function (index, value) {
			if((value.street.search(searchPattern) != -1) && search) {
				output += '<li>' +value.street + '</li>';
				$('#hints').slideDown();
			} 
			if (!search) {
				$('#hints').hide();
			}
		});
		output += '</ul>';
		$('#hints').html(output);
	});
});

$('div').on('click', 'ul li', function () {
	var hint = $(this).text();
	$('#street').val(hint);
	$('#hints').hide();
});

$('#sendData').click(function () {
	var street = $('#street').val();
	var home = $('#home').val();
	var block = $('#block').val();
	var place = $('#place').val();
	var index = $('#index').val();
	
	if (!(street && home && place && index)) {
		$('#errorMessage3').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Не все обязательные поля заполнены.');
		$('#errorWarningNewUser').show();
	} else if (index.length != 6) {
		$('#errorMessage3').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Индекс должен состоять из 6 цифр.');
		$('#errorWarningNewUser').show();
	} else {
		$('#errorWarningNewUser').hide();
		$('#successWarningNewUser').show();
		setTimeout(function() {
			$('#successWarningNewUser').fadeOut('fast')
		}, 2000);
		// далее отправляем значения на сервер
	}
});