<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Test Task Form</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/for-index.css">
</head>
<body>
	<div class="content">
	
		<div id="userForm">
		
			<div id="typeOfUserWrapper">
				<form method="post" class="form-horizontal" id="typeOfUser" novalidate>
					<div class="form-group">
						<div class="form-group">
							<label for="userName" class="col-sm-3 control-label">Ваше имя</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="userName" name="userName" placeholder="Фамилия Имя Отчество" >
								<br>
								<div id="nameWarning" class="alert alert-info" role="alert" style="display:none">
									<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Пожалуйста, укажите ваши полные фамилию, имя, отчество.
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="userBirth" class="col-sm-3 control-label">Дата рождения</label>
							<div class="col-sm-9">
								<input type="date" class="form-control" id="userBirth">
							</div>
						</div>
						<div class="form-group">
							<label for="currentUser" class="col-sm-3 control-label">Ваш статус</label>
							<div class="radio col-sm-9">
							  <label>
								<input type="radio" name="userType" id="currentUser" value="currentUser" >
								Действующий партнер
							  </label>
							</div>
							<div class="radio col-sm-offset-3 col-sm-9">
							  <label>
								<input type="radio" name="userType" id="newUser" value="newUser" >
								Новый пользователь
							  </label>
							</div>
						</div>
						<div class="form-group" id="errorWarning" style="display:none">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="alert alert-danger" role="alert" id="errorMessage1"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<button id="next" type="button" class="btn btn-default btn-lg btn-block">
									Далее <i class="fa fa-arrow-right"></i>
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			
			<div id="currentUserWrapper" style="display:none">
				<form method="post" class="form-horizontal" id="currentUser" novalidate>
					<div class="form-group">
						<div class="form-group">
							<label for="userEmail" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="userEmail" name="userEmail" placeholder="Email" >
							</div>
						</div>
						<div class="form-group">
							<label for="userPass" class="col-sm-3 control-label">Пароль</label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="userPass" name="userPass" placeholder="Пароль" >
							</div>
						</div>
						<div class="form-group" id="errorWarningCurrentUser" style="display:none">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="alert alert-danger" role="alert" id="errorMessage2"></div>
							</div>
						</div>
						<div class="form-group" id="successWarningCurrentUser" style="display:none">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="alert alert-success" role="alert">
									<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Данные успешно отправлены
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<a class="btn btn-default btn-lg btn-block" id="logIn">
									Ок
								</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-default btn-lg btn-block back">
									<i class="fa fa-arrow-left"></i> Назад
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			
			<div id="newUserWrapper" style="display:none">
				<form method="post" class="form-horizontal" id="newUser">
					<div class="form-group">
						<div class="form-group">
							<label for="street" class="col-sm-3 control-label">
								Улица<span>*</span>
							</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="street" name="street" placeholder="Улица" >
								<div id="hints" style="display:none"></div>
							</div>
						</div>
						<div class="form-group">
							<label for="home" class="col-sm-3 control-label">
								Дом<span>*</span>
							</label>
							<div class="col-sm-9 line">
								<input type="text" class="form-control" id="home" name="home" placeholder="Дом" >
								<label for="block" class="control-label">Корпус</label>
								<input type="text" class="form-control" id="block" name="block" placeholder="Корпус">
								<label for="place" class="control-label">
									Офис<span>*</span>
								</label>
								<input type="text" class="form-control" id="place" name="place" placeholder="Офис/Квартира">
							</div>
						</div>
						<div class="form-group">
							<label for="place" class="col-sm-3 control-label">
								Индекс<span>*</span>
							</label>
							<div class="col-sm-9">
								<input type="number" max="6" class="form-control" id="index" name="index" placeholder="Индекс">
							</div>
						</div>
						<div class="form-group" id="errorWarningNewUser" style="display:none">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="alert alert-danger" role="alert" id="errorMessage3"></div>
							</div>
						</div>
						<div class="form-group" id="successWarningNewUser" style="display:none">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="alert alert-success" role="alert">
									<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Данные успешно отправлены
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<a class="btn btn-default btn-lg btn-block" id="sendData">
									Ок
								</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-default btn-lg btn-block back">
									<i class="fa fa-arrow-left"></i> Назад
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			
		</div>
		
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="js/for-index.js"></script>
</body>
</html>